<?php
/*
Plugin Name:VN Content 1 widgets
Plugin URI: http://wordpress.org/plugins/hello-dolly/
Description: This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.
Author: Matt Mullenweg
Version: 1.7.2
Author URI: http://ma.tt/
*/

define( 'CUS_IMG_WID', __FILE__ );

//Add Admin scripts
add_action('admin_enqueue_scripts', 'custom_image_widget_admin_js');
function custom_image_widget_admin_js() {
    wp_enqueue_media();
    wp_enqueue_script('js_script', plugin_dir_url( CUS_IMG_WID ) . '/js/widget.js', false, '1.0', true);
}

//Add Front scripts
add_action('wp_enqueue_scripts', 'custom_image_widget_js');
function custom_image_widget_js() {
    wp_enqueue_media();
    wp_enqueue_script('js_script', plugin_dir_url( CUS_IMG_WID ) . '/js/front.js', false, '1.0', true);
}

function register_sederbar_content_1_widgets() {
    register_widget( 'Conten_1_Widget' );
}
add_action( 'widgets_init', 'register_sederbar_content_1_widgets');

class Conten_1_Widget extends WP_Widget
{
    function __construct() {
        parent::__construct(
            'content_1_widget', 
            __( 'Content1 widget', 'text_domain' ), 
            array( 'description' => __( 'This content1 widget', 'text_domain' ),) // Args
        );
    }
    // thiết lập trường nhập liệu
    function form($instance){
        ?>
            <p>
                <input type="button" class="button button-primary custom_media_button widefat" id="custom_media_button" 
                    name="<?php echo $this->get_field_name('image_btn'); ?>" value="Upload Image" style="margin-top:5px;" />
            </p>
        <?php
        $image = ! empty( $instance['image'] ) ? $instance['image'] : ''; 
        $default = array(
            'title'=>'',
            'content' => '',
            'link' =>'',
            'image'=> '',
        );
        //wp_parse_args() Hàm này có tác dung chuyển dữ liệu từ $default ->$instance
        if( ! empty($instance['image']) ){
            global $wpdb;
            $sql = $wpdb->get_col($wpdb->prepare("SELECT ID FROM wp_posts WHERE guid = '%s';",$instance['image']));
            $file_size = wp_get_attachment_image_src($sql[0],$size = $instance['size']);
            $file = wp_get_attachment_metadata($sql[0]);
            if(!empty($instance['size'])){
                if($instance['size'] == 'full_size' || $instance['size'] == 'large'){
                    $instance['width'] = $file['width'];
                    $instance['height'] = $file['height'];
                }
            }
        } 
        if(!empty($instance['image'])){  
            $display = 'display : block'; 
        }else{ 
            $display = 'display : none'; 
        }

        $instance = wp_parse_args($instance,$default);
        $title =esc_attr($instance['title']);
        $content =esc_attr($instance['content']);
        $link = esc_attr($instance['link']);
       
         ?>
                <div class="content" style="<?php echo $display; ?>">
                    <label for="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>"><?php esc_attr_e( 'Image:', 'text_domain' ); ?></label>
                    <p style="text-align: <?php echo $instance['align']; ?>">
                        <img class="custom_media_image" style="max-width: 300px;" src="<?php if($image){ echo $file_size[0]; } ?>"  
                            width="<?php echo $file['width']; ?>" 
                            height="<?php echo $file['height']; ?>">
                    </p>

                    <input class="widefat custom_media_url" id="<?php echo esc_attr( $this->get_field_id( 'image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'image' ) ); ?>" type="hidden" value="<?php echo esc_attr( $image ); ?>">
                </div>

         <?php
         echo ('Title:<input type ="text" class ="widefat" name ="'.$this->get_field_name('title').'" value ="'.$title.'"/>');
         echo ('Content:<textarea class ="widefat" name ="'.$this->get_field_name('content').'" >'.$content.'</textarea>');
         echo ('Link:<input type ="text" class ="widefat" name ="'.$this->get_field_name('link').'" value ="'.$link.'"/>');
    }

    //lưu dữ liệu tại form
    function update($new_instance,$old_insttance){
        // $instance đại diện giá trị hiên tại của các nhập liệu
        $instance = array();
		$instance['image'] = ( ! empty( $new_instance['image'] ) ) ? sanitize_text_field( $new_instance['image'] ) : '';
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        $instance['content'] = ( ! empty( $new_instance['content'] ) ) ? sanitize_text_field( $new_instance['content'] ) : '';
        $instance['link'] = ( ! empty( $new_instance['link'] ) ) ? sanitize_text_field( $new_instance['link'] ) : '';
        return $instance;

    }

    // hiên thi widget ra ngoài màn hin 
    function widget($args,$instance){

    //  print_r($instance);

       extract($argc);
       //Thay thế cho $before_widget = $args ['before_widget']
       $title =apply_filters('widget_title',$instance['title']);
       echo $before_widget."<article>";

       echo "<figure><img src =".$instance['image']."></figure>";
       echo "<h3><a href='".$instance['link']."'>".$before_title. $title.$after_title."</a></h3>";
       echo "<p>".$instance['content']."</p>";

       echo $after_widget."</article>";

    }

}
