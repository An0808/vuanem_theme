
<?php get_header()?>

        <!-- Header fixed-nav-Wraper_2IX -->
        <div style="min-height: 637px;">
            <main class="homepage_1oO">
                <?php get_sidebar('top-content') ?>

                <!-- sidebar content 1 -->
                 <?php get_sidebar('content-1') ?>
                <!--  -->
                
                <section data-test-id="home_sleeping_section" class="sleeping_1Uu">
                    <div class="container">
                        <figure data-test-id="sleeping_cloud"><img img-id="28" lr="" lr-loader-triggers="screen"
                                lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/mattress.png"
                                lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Sleeping On A Cloud"
                                src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/mattress.png">
                        </figure>
                        <article>
                            <h2>It’s Like Sleeping On A Cloud</h2>
                            <h3>Prices starting at <span>$399</span><del>$499</del></h3>
                            <p>We’ve taken the recent advances in mattress and fabric technology and run with them.
                                Having figured out the optimal levels of firmness, coolness, breathability, and comfort
                                - we put them all into one mattress, making it the best mattress you’ve ever slept on.
                                Period.</p><a id="mattressUVP_shop_mattress" class="button" href="/mattress">Shop
                                Mattress</a>
                        </article>
                    </div>
                </section>
                <section data-test-id="home_benefit_section" class="benefits_1J8">
                    <div class="container">
                        <div class="desktop_2hR">
                            <article>
                                <figure><img img-id="29" lr="" lr-loader-triggers="screen"
                                        lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/cool.svg"
                                        lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                                        alt="Tencel Cooling Cover"
                                        src="https://media.nectarsleep.com/nectarsleep/home/cool.svg"></figure>
                                <h3>Tencel Cooling Cover</h3>
                                <p>Nectar’s Cooling Cover with Tencel™ is moisture-wicking, more breathable, and softer
                                    than standard cotton covers</p>
                            </article>
                            <article>
                                <figure><img img-id="30" lr="" lr-loader-triggers="screen"
                                        lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/gel_icon.svg"
                                        lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Gel Memory Foam"
                                        src="https://media.nectarsleep.com/nectarsleep/home/gel_icon.svg"></figure>
                                <h3>Gel Memory Foam</h3>
                                <p>Two layers of Gel Memory Foam help Nectar circulate air, distribute weight and
                                    contour to your body.</p>
                            </article>
                            <article>
                                <figure><img img-id="31" lr="" lr-loader-triggers="screen"
                                        lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/sun_icon.svg"
                                        lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Wake Up Rested"
                                        src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/sun_icon.svg">
                                </figure>
                                <h3>Wake Up Rested</h3>
                                <p>Whether you’re a back, side or front sleeper, Nectar molds to your body, providing
                                    ultimate support.</p>
                            </article>
                        </div>
                    </div>
                </section>
                <div class="">
                    <div class="heading_3gt">
                        <h2>Award-Winning Layers Of Comfort</h2>
                        <p>The Most Comfortable Mattress, for the best price</p>
                    </div>
                    <div class="awards_2fd">
                        <figure class="award_1iS">
                            <div class="award__img_1xN"><img
                                    src="https://media.nectarsleep.com/nectarsleep/PDP-redesign-a/fancy-tuck.png?auto=webp&amp;width=150"
                                    alt="fancy-tuck" title="fancy-tuck"></div>
                            <p class="award__title_u0r">Tuck Award</p>
                            <p class="award__text_2al">Best Value Memory Foam Mattress</p>
                            <p class="award__year_16u">2019</p>
                        </figure>
                        <figure class="award_1iS">
                            <div class="award__img_1xN"><img
                                    src="https://media.nectarsleep.com/nectarsleep/PDP-redesign-a/mattress-advisor.png?auto=webp&amp;width=150"
                                    alt="mattress-advisor" title="mattress-advisor"></div>
                            <p class="award__title_u0r">Mattress Advisor</p>
                            <p class="award__text_2al">Best Value Mattress</p>
                            <p class="award__year_16u">2019</p>
                        </figure>
                        <figure class="award_1iS">
                            <div class="award__img_1xN"><img
                                    src="https://media.nectarsleep.com/nectarsleep/PDP-redesign-a/usa-today.jpeg?auto=webp&amp;width=150"
                                    alt="usa-today" title="usa-today"></div>
                            <p class="award__title_u0r">USA Today</p>
                            <p class="award__text_2al">Best Mattress Of The Year</p>
                            <p class="award__year_16u">2019</p>
                        </figure>
                        <figure class="award_1iS">
                            <div class="award__img_1xN"><img
                                    src="https://media.nectarsleep.com/nectarsleep/PDP-redesign-a/slumber-yard.png?auto=webp&amp;width=150"
                                    alt="slumber-yard" title="slumber-yard"></div>
                            <p class="award__title_u0r">Slumber Yard</p>
                            <p class="award__text_2al">Best Foam Mattress</p>
                            <p class="award__year_16u">2019</p>
                        </figure>
                        <figure class="award_1iS">
                            <div class="award__img_1xN"><img
                                    src="https://media.nectarsleep.com/nectarsleep/hp/reviewed-badge-best-overall-v1.png?auto=webp&amp;width=150"
                                    alt="best-overall" title="best-overall"></div>
                            <p class="award__title_u0r">Best Overall</p>
                            <p class="award__text_2al">Best Overall Mattress</p>
                            <p class="award__year_16u">2020</p>
                        </figure>
                    </div>
                </div>

               <!-- sidebar content -->
               <?php get_sidebar('content') ?>

               <!-- sideber content -->
                <section data-test-id="home_risk_free_section" class="risk-free-trial_3Yw">
                    <div class="container">
                        <div class="blurb_IuK right_mzD">
                            <article>
                                <h2>Try Our 100%<br> Risk-Free 365-Night<br> Home Trial</h2>
                                <p><span>Additional <a href="/mattress-trial">terms &amp; conditions</a> apply.</span>
                                </p><a class="button" id="home_trial_shop_now" href="/mattress">Shop Now</a>
                            </article>
                            <figure><img img-id="32" lr="" lr-loader-triggers="screen:+400"
                                    lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/trial.jpg?auto=webp"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                                    alt="100% Risk-Free Trial" title="100% Risk-Free Trial"
                                    src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/trial.jpg?auto=webp">
                            </figure>
                        </div>
                    </div>
                </section>
                <section data-test-id="home_comparetable_section" class="compare-table_1dh">
                    <div class="container">
                        <h2>We Make Mattress Shopping Simple</h2>
                        <p>Comfort, convenience and peace of mind—all at an unbeatable price.</p>
                        <table>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nectar</th>
                                    <th>Casper</th>
                                    <th>Purple</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Sleep Trial</td>
                                    <td>365 Nights</td>
                                    <td>100 nights</td>
                                    <td>100 nights</td>
                                </tr>
                                <tr>
                                    <td>Warranty</td>
                                    <td>Forever Warranty™</td>
                                    <td>10 Years</td>
                                    <td>10 Years</td>
                                </tr>
                                <tr>
                                    <td>Free Shipping*</td>
                                    <td data-test-id="returns_Nectar_shipping"><img img-id="33" lr=""
                                            lr-loader-triggers="screen"
                                            lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check-circle.svg"
                                            lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Nectar-logo"
                                            style="width: 45px;"
                                            src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check-circle.svg">
                                    </td>
                                    <td data-test-id="returns_Casper_shipping"><img img-id="34" lr=""
                                            lr-loader-triggers="screen"
                                            lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check.svg"
                                            lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Casper-logo"
                                            style="width: 24px;"
                                            src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check.svg">
                                    </td>
                                    <td data-test-id="returns_Purple_shipping"><img img-id="35" lr=""
                                            lr-loader-triggers="screen"
                                            lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check.svg"
                                            lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Purple-logo"
                                            style="width: 24px;"
                                            src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check.svg">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Free Returns</td>
                                    <td data-test-id="returns_Nectar_returns"><img img-id="36" lr=""
                                            lr-loader-triggers="screen"
                                            lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check-circle.svg"
                                            lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Nectar-logo"
                                            style="width: 45px;"
                                            src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check-circle.svg">
                                    </td>
                                    <td data-test-id="returns_Casper_returns"><img img-id="37" lr=""
                                            lr-loader-triggers="screen"
                                            lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check.svg"
                                            lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Casper-logo"
                                            style="width: 24px;"
                                            src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check.svg">
                                    </td>
                                    <td data-test-id="returns_Purple_returns"><img img-id="38" lr=""
                                            lr-loader-triggers="screen"
                                            lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check.svg"
                                            lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="Purple-logo"
                                            style="width: 24px;"
                                            src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/check.svg">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Price (Queen)</td>
                                    <td>
                                        <div class="formatted-price_2DL"><span>$699</span><del>$799</del></div>
                                    </td>
                                    <td>$1,095+</td>
                                    <td>$1,099+</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th><a id="compare_grid_shop_mattress" class="button" href="/mattress">Shop
                                            Mattress</a></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table><a id="compare_grid_shop_mattress" class="button" href="/mattress">Shop Mattress</a>
                        <p class="disclaimer-copy_29H">*Shipping is free of charge for orders being delivered within the
                            contiguous US. Shipping fees will apply for orders being delivered to AK, HI, Guam, etc. and
                            are non-refundable. Price may not reflect any current promotions or discount. Last Updated:
                            Nov 2019.</p>
                    </div>
                </section>
                <section data-test-id="loved_products_section" class="products_Pbn">
                    <div class="container">
                        <h2>Check Out Our Most Loved Bedding Set</h2>
                        <div class="grid_3q3">
                            <article data-test-id="grid_foundation-with-legs"><img img-id="39" lr=""
                                    lr-loader-triggers="screen"
                                    lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/foundation.jpg?auto=wepb"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                                    alt="foundation-with-legs"
                                    src="https://media.nectarsleep.com/nectarsleep/home/foundation.jpg?auto=wepb">
                                <h3>The nectar Mattress foundation</h3>
                                <h4><span><span>$200 - </span>$275</span></h4><a id="merch_general_foundation_shop_now"
                                    class="button_3Uy" href="/mattress-foundation">Shop Now</a>
                            </article>
                            <article data-test-id="grid_the-standard-textile-sheet-set"><img img-id="40" lr=""
                                    lr-loader-triggers="screen"
                                    lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/sheets.jpg?auto=webp"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                                    alt="the-standard-textile-sheet-set"
                                    src="https://media.nectarsleep.com/nectarsleep/home/sheets.jpg?auto=webp">
                                <h3>Luxury Sateen Bed sheet set</h3>
                                <h4></h4><a id="merch_general_sheets_shop_now" class="button_3Uy" href="/sheets">Shop
                                    Now</a>
                            </article>
                            <article data-test-id="grid_the-nectar-mattress-protector"><img img-id="41" lr=""
                                    lr-loader-triggers="screen"
                                    lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/protector.jpg?auto=webp"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                                    alt="the-nectar-mattress-protector"
                                    src="https://media.nectarsleep.com/nectarsleep/home/protector.jpg?auto=webp">
                                <h3>The nectar mattress protector</h3>
                                <h4><span><span>$79 - </span>$99</span></h4><a
                                    id="merch_general_mattress_protector_shop_now" class="button_3Uy"
                                    href="/mattress-protector">Shop Now</a>
                            </article>
                        </div>
                    </div>
                </section>
                <section data-test-id="home_satisfied_customers_section" class="container">
                    <div class="satisfied-customers_2YA">
                        <h2><span class="">750,000</span>+ Happy Sleepers </h2>
                        <p>Average Rating of 4.5</p>
                        <div class="platforms_2MS">
                            <article data-test-id="platforms_0"><span style="vertical-align: middle;"><i><svg
                                            xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i></span>
                                <h3>1,000+ Reviews</h3><img img-id="67" lr="" lr-loader-triggers="screen"
                                    lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/amazon-logo.svg"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="amazon"
                                    title="amazon"
                                    src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/amazon-logo.svg">
                            </article>
                            <article data-test-id="platforms_1"><span style="vertical-align: middle;"><i><svg
                                            xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i></span>
                                <h3>2,200+ Reviews</h3><img img-id="68" lr="" lr-loader-triggers="screen"
                                    lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/trusted-store.png"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                                    alt="google trusted store" title="google trusted store"
                                    src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/trusted-store.png">
                            </article>
                            <article data-test-id="platforms_2"><span style="vertical-align: middle;"><i><svg
                                            xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 42 40" fill="#001780">
                                            <path
                                                d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                                                stroke="#001780"></path>
                                        </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14"
                                            viewBox="0 0 84 82" fill="transparent">
                                            <g>
                                                <path
                                                    d="M41.7479 5.10905L50.6188 32.431L50.8432 33.1222H51.5699H80.2788L57.0522 50.0098L56.4649 50.4368L56.6892 51.1274L65.5605 78.4509L42.336 61.5648L41.7479 61.1372L41.1598 61.5648L17.9353 78.4509L26.8067 51.1274L27.0309 50.4368L26.4436 50.0098L3.21702 33.1222H31.9259H32.6526L32.877 32.431L41.7479 5.10905Z"
                                                    stroke="#001780"></path>
                                                <path
                                                    d="M41 60.504L19.0204 77.2085L27.9454 51.326L28.1878 50.623L27.5843 50.1885L5.10033 34H32H32.713L32.9454 33.326L41 9.96757V60.504Z"
                                                    fill="#001780" stroke="#001780"></path>
                                            </g>
                                        </svg></i></span>
                                <h3>24,200+ Reviews</h3><img img-id="69" lr="" lr-loader-triggers="screen"
                                    lr-loader-actions="image:https://media.nectarsleep.com/navigation/nectar/logo.svg"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="nectar"
                                    title="nectar" src="https://media.nectarsleep.com/navigation/nectar/logo.svg">
                            </article>
                        </div>
                    </div>
                </section>
                <section class="stores-section_3kb">
                    <div class="stores-container_2Of">
                        <div class="content-wrap_114">
                            <div class="fadeInDown-element_C8U fade-in-down__start_2lq fade-in-down__end_2jF" lr=""
                                lr-revealer-triggers="screen"
                                lr-revealer-actions="addClassesDelay:0:fade-in-down__end_2jF"><svg width="49"
                                    height="70" viewBox="0 0 49 70" fill="none" xmlns="http://www.w3.org/2000/svg"
                                    class="stores-section__marker_3nR">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M0 23.4584C0 10.5034 10.8354 0 24.2 0C37.5646 0 48.4 10.5034 48.4 23.4584C48.4 42.7941 24.2 69.1429 24.2 69.1429C24.2 69.1429 0 42.3554 0 23.4584ZM24.1996 34.5691C30.8823 34.5691 36.2996 29.4097 36.2996 23.0453C36.2996 16.6809 30.8823 11.5215 24.1996 11.5215C17.517 11.5215 12.0996 16.6809 12.0996 23.0453C12.0996 29.4097 17.517 34.5691 24.1996 34.5691Z"
                                        fill="#001780"></path>
                                </svg></div>
                            <div class="FadeIn-element_2kh fade-in__start_3ec fade-in__end_LYe" lr=""
                                lr-revealer-triggers="middle" lr-revealer-actions="addClassesDelay:0:fade-in__end_LYe">
                                <svg width="28" height="10" viewBox="0 0 28 10" fill="none"
                                    xmlns="http://www.w3.org/2000/svg" class="stores-section__marker-shadow_33L">
                                    <g opacity="0.3">
                                        <ellipse cx="13.6481" cy="5.38895" rx="13.4444" ry="4.48148" fill="#001780">
                                        </ellipse>
                                    </g>
                                </svg></div>
                            <h2 class="content__title_2sy">
                                <div class="fadeInUp-element_X90 fade-in-up__start_2Si fade-in-up__end_U60" lr=""
                                    lr-revealer-triggers="screen"
                                    lr-revealer-actions="addClassesDelay:0:fade-in-up__end_U60">Now Available In</div>
                                <div class="fadeInUp-element_X90 fade-in-up__start_2Si fade-in-up__end_U60" lr=""
                                    lr-revealer-triggers="screen"
                                    lr-revealer-actions="addClassesDelay:150:fade-in-up__end_U60">Stores Nationwide
                                </div>
                            </h2>
                            <h3 class="content__sub-title_2Fv">
                                <div class="fadeInUp-element_X90 fade-in-up__start_2Si fade-in-up__end_U60" lr=""
                                    lr-revealer-triggers="screen"
                                    lr-revealer-actions="addClassesDelay:250:fade-in-up__end_U60">Want to try Nectar in
                                    person?</div>
                                <div class="fadeInUp-element_X90 fade-in-up__start_2Si fade-in-up__end_U60" lr=""
                                    lr-revealer-triggers="screen"
                                    lr-revealer-actions="addClassesDelay:500:fade-in-up__end_U60">Now available in 1500
                                    stores.</div>
                            </h3>
                            <div class="fadeZoomIn-element_3Wf fade-zoom-in__start_Vb9 fade-zoom-in__end_XDe" lr=""
                                lr-revealer-triggers="screen"
                                lr-revealer-actions="addClassesDelay:0:fade-zoom-in__end_XDe"><a class="button_CtO"
                                    id="store_locator_find_a_store" href="/store-locator">Find a Store Near You</a>
                            </div>
                        </div>
                        <div class="image-wrap_3CY">
                            <div class="FadeIn-element_2kh fade-in__start_3ec fade-in__end_LYe" lr=""
                                lr-revealer-triggers="middle"
                                lr-revealer-actions="addClassesDelay:150:fade-in__end_LYe"><img img-id="42" lr=""
                                    lr-loader-triggers="screen"
                                    lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/HP-redesign-a/stores-map-v3.png?auto=webp"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc"
                                    class="image-wrap__map_3bO" alt="Stores"
                                    src="https://media.nectarsleep.com/nectarsleep/HP-redesign-a/stores-map-v3.png?auto=webp">
                            </div>
                        </div>
                    </div>
                </section>
                <section data-test-id="home_enjoysleep_section" class="enjoy-sleep_1om">
                    <div class="container">
                        <div class="blurb_IuK left_11q">
                            <figure><img img-id="43" lr="" lr-loader-triggers="screen:+400"
                                    lr-loader-actions="image:https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/enjoy.jpg?auto=webp"
                                    lr-revealer-triggers="screen" lr-revealer-actions="setSrc" alt="enjoy your sleep"
                                    title="enjoy your sleep"
                                    src="https://media.nectarsleep.com/nectarsleep/home/hp-redesign-b/enjoy.jpg?auto=webp">
                            </figure>
                            <article>
                                <h2>Are You Ready To Enjoy Your Best Sleep?</h2><a class="button"
                                    id="best_sleep_shop_now" href="/mattress">Shop Now</a>
                            </article>
                        </div>
                    </div>
                </section>
                <section data-test-id="newsletter_section" class="newsletter_2ZC">
                    <div class="container">
                        <h2>Catch More ZZZ’s</h2>
                        <p>Sign up here to receive exclusive deals, details, and the latest newzzz about our products.
                        </p>
                        <form class=""><input type="email" id="newsletter_email" placeholder="Email Address"
                                required=""><button class="button_1IL">Sign Up</button></form>
                    </div>
                </section>
            </main>
        </div>
<?php get_footer() ?>