<!DOCTYPE html>
<html <?php language_attributes() ?> >
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <title>
        <?php 
            wp_title('|',true,'right');
            bloginfo('name');
        ?>
    </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="pingback" href="<?php bloginfo('pingback_url');?>"/>
    <!-- wp_head nạp cac tập tin css js  -->    
    <?php wp_head() ?>
     <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url') ;?>"/>
    
    <?php

        //thêm 1 đoạn cả html
        if ( function_exists( 'the_custom_logo' ) ) {
            the_custom_logo();
        }

        //lấy url của ảnh 
        // $custom_logo_id = get_theme_mod( 'custom_logo' );
        // $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        // echo $image[0];

        $img = get_bloginfo('template_url').'/style/images/logo/vuanem_logo.png';
        $custom_logo_id = get_theme_mod('custom_logo');

                            $logo =wp_get_attachment_image_src($custom_logo_id,'full');
                            if(has_custom_logo()){
                                $img =esc_url($logo[0]);
                            }
            // print_r($logo) ;
            // echo $img ;

    ?>
    
    <style type="text/css">
        .nectar-nav_3DA .desktop-nav_2Xc .desktop-nav-logo_2aY {
            width: 110px;
            background: url(<?php echo $img ?>) 50%/contain no-repeat;
        }
        .theme-nectar-home-page_TkL.hero-section_qRg .image-block_1fB {
            background: url(<?php header_image() ?>) no-repeat;
            background-size: cover;
            background-position: 50% 25%;
        }
    </style>

 
</head>

<body data-wz_page_group="home" sr-scroll-left="0" sr-scroll-top="143" style="">

    <div id="app">
        <div id="cart_alert" class="cart-alert-box_5T_ small_1Jg"></div>
        <div class="fixed-nav-wrapper_2IX">
           <div class="top-banner-bar_3KD top-banner-bar--flex_2OR" style="background-color: rgb(102, 119, 204); color: rgb(255, 255, 255);"><div class="top-banner-bar-inner_2fn"><span class="close_36B"></span><div class="bar-content_2ik"><span><span class="title_1dG">Presidents Day Sale: $100 Off Every Mattress </span></span><span class="terms-conditions_d6B">&nbsp;<a target="_blank" rel="noopener noreferrer" href="https://www.nectarsleep.com/p/promo-details/">*Terms &amp; Conditions Apply</a></span><div class="form_2Om"><div class="inline-newsletter-form_YiU"><form class=""><input type="email" id="email" placeholder="Enter Your Email" required=""><button class="button_1kA">Get Offer</button></form></div></div><div class="count-down-wrap_16j"></div></div></div></div>
            <div id="header-nav" playground-hidden="" class="nectar-nav_3DA desktop-nav-wrapper_1tY">
                <div class="desktop-nav_2Xc">
                <!-- kiểm tra xem nó có phải trang chủ ko dùng hàm is_home() -->
                <?php
                
                    printf('<a class="desktop-nav-logo_2aY" data-test-id="nectar_logo" href="%1$s" titel="%2$s"></a>',get_bloginfo('url'),get_bloginfo('description'));
                ?>
                
                 <!--left menu  -->
                        <?php get_sidebar('left-menu') ?>
                 <!-- left menu -->
                 

                <!-- right menu -->
                <?php get_sidebar('right-menu')?>
                <!-- right menu -->
            </div>
        </div>
        <div playground-hidden="" style="width: 100%; height: 110px;"></div>
        <div id="yieldify_placeholder" class="nectar_2ko"></div>