<?php
 /*
    con file js 
 */
//     file Functions.php sẽ được tự nạp vào theme khi thực hiện 
//   nạp vào hệ thống 
/* nạp css vào theme sử dụng là add_action */
// biên đầu (wp_enqueue_scripts ,function() để sử lý gửi linh vào  là tên chuyền vào 
add_action('wp_enqueue_scripts','vuanem_theme_register_style');

function vuanem_theme_register_style(){
    //get_template_directory_uri() hàm lấy ra đường dẫn url  http://wordpress.test/wp-content/themes/vuanem/css
        $cssUrlAssets = get_template_directory_uri().'/css/assets';
        $cssUrlPublic = get_template_directory_uri().'/css/public';
        // echo $cssUrlAssets;
        // nap file css Assets 
        wp_register_style('vuanem_theme_app_nectar',$cssUrlAssets.'/196a82ccb81240aab9eb8652252c49aa-app-nectar.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_app_nectar');

        wp_register_style('vuanem_theme_nectarFonts',$cssUrlAssets.'/b4a131c95daca8fbde36e2f78fc05646-nectarFonts.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_nectarFonts');

        // nap file css As public 

        wp_register_style('vuanem_theme_public_13', $cssUrlPublic.'/13-nectar.4c91a619e93ab165b45b.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_public_13');

        wp_register_style('vuanem_theme_public_60', $cssUrlPublic.'/60-nectar.ae3ef1cc6f04d2a18054.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_public_60');

        wp_register_style('vuanem_theme_public_66', $cssUrlPublic.'/66-nectar.e2665daee661ebfa47ca.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_public_66');

        wp_register_style('vuanem_theme_public_106', $cssUrlPublic.'/106-nectar.b54ff03cfc5cfc9ea660.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_public_106');

        wp_register_style('vuanem_theme_public_137', $cssUrlPublic.'/137-nectar.afb63e418539a416cadf.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_public_137');
        
        wp_register_style('vuanem_theme_public_155', $cssUrlPublic.'/155-nectar.65a8ab4a387513f3b1f9.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_public_155');

        wp_register_style('vuanem_theme_public_160', $cssUrlPublic.'/160-nectar.e19b64a531df661db036.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_public_160');

        wp_register_style('vuanem_theme_public_166', $cssUrlPublic.'/166-nectar.451c447c0619a2f80cba.min.css',array(),'1.0');
        wp_enqueue_style('vuanem_theme_public_166');

        // wp_register_style('vuanem_theme_public_15', $cssUrlPublic.'/15-nectar.f2e0ac8df25b3f1feac4.min.css',array(),'1.0');
        // wp_enqueue_style('vuanem_theme_public_15');

        // wp_register_style('vuanem_theme_public_104', $cssUrlPublic.'/104-nectar.b54ff03cfc5cfc9ea660.min.css',array(),'1.0');
        // wp_enqueue_style('vuanem_theme_public_104');
        
        // wp_register_style('vuanem_theme_public_129', $cssUrlPublic.'/129-nectar.afb63e418539a416cadf.min.css',array(),'1.0');
        // wp_enqueue_style('vuanem_theme_public_129');

        // wp_register_style('vuanem_theme_public_159', $cssUrlPublic.'/159-nectar.451c447c0619a2f80cba.min.css',array(),'1.0');
        // wp_enqueue_style('vuanem_theme_public_159');
}


// /////////////////////////////////////////
// Tìm nạp file js 

 add_action('wp_enqueue_scripts','vuanem_theme_register_js');
function vuanem_theme_register_js(){
    wp_register_script('vuanem_theme_js_js', get_template_directory_uri().'/js/js.js',array(),'1.0',false);
    wp_enqueue_script('vuanem_theme_js_js');
   
}



//Adding theme support 
function gt_init (){
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');
    add_theme_support('html5',
    array('comment-list','comment-form','search-form')    
);
}

add_action('after_setup_theme','gt_init');


// thêm 1 thẻ functuon  project 
// function gt_custom_post_type(){
//     register_post_type('project',
//         array(
//             'rewrite' => array('slug' => 'project'),
//             'labels' =>array(
//                 'name'=>'Project',
//                 'singular_name' =>'Project',
//                 'add_new_item' =>'Add  Project'
//             ),
//             'menu-icon' =>'dashicons-clipboard',
//             'public'  => true,
//             'has_archive' => true,
//             'supports' =>array(
//                 'title','thumbnail','editor','excerpt','comments'
//             )
//         )
//     );
// }

add_action('init','gt_custom_post_type');

// add_theme_support('menus');

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu-left' => __( 'Header Menu left' ),
      'header-menu' => __( 'Header Menu right' ),
      'extra-menu' => __( 'Extra Menu' ),
      'footer' => __( 'Footer Menu' ),
     
     )
   );
 }
 add_action( 'init', 'register_my_menus' );


/**
@ Thiết lập hàm hiển thị logo
**/



function themename_custom_logo_setup(){
  $defaults =array(
      'flex-height' =>true,
      'flex-width' =>true,
      'header-texr' => array('site-title','site-description')

  );
  add_theme_support('custom-logo',$defaults);
}
 add_action('after_setup_theme','themename_custom_logo_setup');


  // add_filter("nav_menu_link_attributes","owt_each_anchor_attr");
  // function owt_each_anchor_attr($attr){
  //   $attr["class"] ="extra-link_3UH";
  //   return $attr;
  // }


  function register_my_project(){

        register_post_type('custom_projec',array(
            'labels' =>array(
                'name'=> __('Our Project'),
                'singular_name' => __('custom_name')
            ),
            'public' =>true,
            'show_in_nav_menus' =>true,
            'has_archive' =>false,
            'supports' =>array('title','editor','excerpt', 'author', 'comments', 'revisions', 'custom-fields')
        )
    );
  }

add_action('init','register_my_project');


//  widgets 
function profotech_register_sideber(){
    
    register_sidebar(array(
        'name'=> __('Sidebar content 1','theme_vuanem'),
        'id' => 'sidebar-content-1',
    )
  );
  register_sidebar(array(
    'name' => __('Top Content','theme_vuanem'),
    'id' => 'top-content-img',
    'before_widget'=> '<div class="image-block__badge_3xR image-block__badge--best-overall_3oN">',
    'after_widget' => '</div>'
  )
);
  


}

add_action('widgets_init','profotech_register_sideber');

    $default = array(
      'default-image'          => '',
      'random-default'         => false,
      'width'                  => 1000,
      'height'                 => 1000,
      'flex-height'            => true,
      'flex-width'             => true,
      'default-text-color'     => '',
      'header-text'            => true,
      'uploads'                => true,
      'wp-head-callback'       => '',
      'admin-head-callback'    => '',
      'admin-preview-callback' => '',
      'video'                  => false,
      'video-active-callback'  => 'is_front_page',
  );
  add_theme_support( 'custom-header', $default );
