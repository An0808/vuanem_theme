<section id="home_banner_section" class="hero-section_qRg theme-nectar-home-page_TkL">
    <div class="hero-section__image-block_Tr1">
        <div class="image-block_1fB">
            <div class="image-block__badges_2nE">
                <?php
                    if(is_active_sidebar('top-content-img')){
                        dynamic_sidebar('top-content-img');
                    }
                ?>
            </div>
        </div>
    </div>
    <div class="hero-section__info-block_1F3"><svg width="65" height="65" viewBox="0 0 65 65" fill="none"
            xmlns="http://www.w3.org/2000/svg" class="star_2VV star--large_1BM star--large-1_2J4">
            <path
                d="M44.3427 11.8816L30.2271 22.5659L14.6862 15.7217L19.6584 32.358L7.85514 45.6942L25.055 45.2888L33.3039 60.387L38.9517 43.497L55.8453 39.4849L42.1476 29.4482L44.3427 11.8816Z"
                fill="white" fill-opacity="0.32"></path>
        </svg><svg width="65" height="65" viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg"
            class="star_2VV star--large_1BM star--large-2_hDu">
            <path
                d="M44.3427 11.8816L30.2271 22.5659L14.6862 15.7217L19.6584 32.358L7.85514 45.6942L25.055 45.2888L33.3039 60.387L38.9517 43.497L55.8453 39.4849L42.1476 29.4482L44.3427 11.8816Z"
                fill="white" fill-opacity="0.32"></path>
        </svg><svg width="65" height="65" viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg"
            class="star_2VV star--large_1BM star--large-3_1ud">
            <path
                d="M44.3427 11.8816L30.2271 22.5659L14.6862 15.7217L19.6584 32.358L7.85514 45.6942L25.055 45.2888L33.3039 60.387L38.9517 43.497L55.8453 39.4849L42.1476 29.4482L44.3427 11.8816Z"
                fill="white" fill-opacity="0.32"></path>
        </svg><svg width="65" height="65" viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg"
            class="star_2VV star--large_1BM star--large-4_FKi">
            <path
                d="M44.3427 11.8816L30.2271 22.5659L14.6862 15.7217L19.6584 32.358L7.85514 45.6942L25.055 45.2888L33.3039 60.387L38.9517 43.497L55.8453 39.4849L42.1476 29.4482L44.3427 11.8816Z"
                fill="white" fill-opacity="0.32"></path>
        </svg><svg width="65" height="65" viewBox="0 0 65 65" fill="none" xmlns="http://www.w3.org/2000/svg"
            class="star_2VV star--large_1BM star--large-5_NNk">
            <path
                d="M44.3427 11.8816L30.2271 22.5659L14.6862 15.7217L19.6584 32.358L7.85514 45.6942L25.055 45.2888L33.3039 60.387L38.9517 43.497L55.8453 39.4849L42.1476 29.4482L44.3427 11.8816Z"
                fill="white" fill-opacity="0.32"></path>
        </svg><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"
            class="star_2VV star--small_3HG star--small-1_2lX">
            <path
                d="M20.0104 5.36177L13.6405 10.1832L6.62738 7.0947L8.87116 14.6021L3.54473 20.6203L11.3065 20.4374L15.0289 27.2507L17.5776 19.6288L25.2011 17.8182L19.0198 13.289L20.0104 5.36177Z"
                fill="white" fill-opacity="0.32"></path>
        </svg><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"
            class="star_2VV star--small_3HG star--small-2_qsC">
            <path
                d="M20.0104 5.36177L13.6405 10.1832L6.62738 7.0947L8.87116 14.6021L3.54473 20.6203L11.3065 20.4374L15.0289 27.2507L17.5776 19.6288L25.2011 17.8182L19.0198 13.289L20.0104 5.36177Z"
                fill="white" fill-opacity="0.32"></path>
        </svg><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"
            class="star_2VV star--small_3HG star--small-3_26v">
            <path
                d="M20.0104 5.36177L13.6405 10.1832L6.62738 7.0947L8.87116 14.6021L3.54473 20.6203L11.3065 20.4374L15.0289 27.2507L17.5776 19.6288L25.2011 17.8182L19.0198 13.289L20.0104 5.36177Z"
                fill="white" fill-opacity="0.32"></path>
        </svg><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"
            class="star_2VV star--small_3HG star--small-4_2LR">
            <path
                d="M20.0104 5.36177L13.6405 10.1832L6.62738 7.0947L8.87116 14.6021L3.54473 20.6203L11.3065 20.4374L15.0289 27.2507L17.5776 19.6288L25.2011 17.8182L19.0198 13.289L20.0104 5.36177Z"
                fill="white" fill-opacity="0.32"></path>
        </svg>
        <div class="info_Mi1">
            <h1 class="info__title_3iU">750,000+ Happy Sleepers Have Chosen Nectar</h1>
            <div class="info__cta-card_2nz">
                <div class="cta-card_1Am theme-nectar-home-page_3Bx">
                    <div class="title_3C_"><svg width="31" height="31" viewBox="0 0 31 31" fill="none"
                            xmlns="http://www.w3.org/2000/svg" class="title__icon_1Hr">
                            <path
                                d="M24.1866 9.30475C25.5626 9.30475 26.678 8.18927 26.678 6.81326C26.678 5.43725 25.5626 4.32178 24.1866 4.32178C22.8105 4.32178 21.6951 5.43725 21.6951 6.81326C21.6951 8.18927 22.8105 9.30475 24.1866 9.30475Z"
                                stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round">
                            </path>
                            <path
                                d="M17.8725 1.29289L1.70711 17.4583C1.31658 17.8489 1.31658 18.482 1.70711 18.8725L12.1114 29.2768C12.5081 29.6736 13.1536 29.6663 13.5413 29.2608L29.7228 12.3355C29.9007 12.1494 30 11.9019 30 11.6445V2C30 1.44772 29.5523 1 29 1H18.5797C18.3144 1 18.0601 1.10536 17.8725 1.29289Z"
                                stroke-width="2"></path>
                            <line x1="1" y1="-1" x2="8.39598" y2="-1"
                                transform="matrix(0.707109 0.707104 -0.707109 0.707104 13.3901 10.9653)"
                                stroke-width="2" stroke-linecap="round"></line>
                            <line x1="1" y1="-1" x2="8.39598" y2="-1"
                                transform="matrix(0.707104 0.707109 -0.707104 0.707109 10.0681 14.2881)"
                                stroke-width="2" stroke-linecap="round"></line>
                            <line x1="1" y1="-1" x2="8.39598" y2="-1"
                                transform="matrix(0.707108 0.707106 -0.707108 0.707106 6.74609 17.6104)"
                                stroke-width="2" stroke-linecap="round"></line>
                        </svg>
                        <h2 class="title__text_2wr">SHOP THE SALE:</h2>
                    </div>
                    <div class="cta-card__points_3Te">
                        <div class="point_3SB"><svg width="5" height="5" viewBox="0 0 5 5" fill="none"
                                xmlns="http://www.w3.org/2000/svg" class="point__bullet_2BC">
                                <circle opacity="0.6" cx="2.5" cy="2.5" r="2.5"></circle>
                            </svg><span class="point__text_b94">Presidents Day Sale: $100 Off Every
                                Mattress</span></div>
                        <div class="point_3SB"><svg width="5" height="5" viewBox="0 0 5 5" fill="none"
                                xmlns="http://www.w3.org/2000/svg" class="point__bullet_2BC">
                                <circle opacity="0.6" cx="2.5" cy="2.5" r="2.5"></circle>
                            </svg><span class="point__text_b94">2 Memory Foam Pillows Included with
                                Nectar Mattress ($150 Value)*</span></div>
                    </div>
                    <div class="cta-card__button_NiW"><a class="cta-button_1G1" id="hero_shop_mattress"
                            href="/mattress">Shop Mattress</a></div>
                    <div class="cta-card__offer-expiration_LIF">Sale ends Feb 24, 2020, 6AM EST</div>
                </div><img class="info__cta-card-image-top_HMC"
                    src="https://media.nectarsleep.com/nectarsleep/HP-redesign-a/cloud-purple-v1.svg"
                    alt="mattress-advisor" style="top: 20px; left: -55px;"><img class="info__cta-card-image-bottom_MD0"
                    src="https://media.nectarsleep.com/nectarsleep/HP-redesign-a/cloud-white-v1.svg"
                    alt="mattress-advisor" style="bottom: -5px; right: -35px;">
            </div>
        </div>
    </div>
</section>