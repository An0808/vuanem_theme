<?php get_header();?>
<div>
    <?php 

        if(have_posts()){
            while(have_posts()){
                the_post();

                ?>
                <div style="text-align: center;" class="myClass">
                    <h3><a href="<?php echo get_the_permalink() ?>"> <?php the_title()  ?></a> </h3>
                    <p><?php the_content() ?></p>
                </div>
                <?php
            }
        }

    ?>

</div>

<?php get_footer() ?>