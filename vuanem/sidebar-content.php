<section data-test-id="home_customer_reviews_section" class="reviews_1N1">
    <h2>Read what our customers are saying</h2><span style="vertical-align: middle;"><i><svg
                xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 42 40" fill="#001780">
                <path
                    d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                    stroke="#001780"></path>
            </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 42 40"
                fill="#001780">
                <path
                    d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                    stroke="#001780"></path>
            </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 42 40"
                fill="#001780">
                <path
                    d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                    stroke="#001780"></path>
            </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 42 40"
                fill="#001780">
                <path
                    d="M20.874 1.77425L25.3094 15.4352L25.4216 15.7808H25.785H40.1394L28.5261 24.2246L28.2325 24.4381L28.3446 24.7834L32.7803 38.4452L21.168 30.0021L20.874 29.7883L20.5799 30.0021L8.96765 38.4452L13.4033 24.7834L13.5155 24.4381L13.2218 24.2246L1.60851 15.7808H15.9629H16.3263L16.4385 15.4352L20.874 1.77425Z"
                    stroke="#001780"></path>
            </svg></i><i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 84 82"
                fill="transparent">
                <g>
                    <path
                        d="M44.2521 3.5485L53.123 30.8705L53.3474 31.5617H54.0741H82.783L59.5564 48.4492L58.9691 48.8762L59.1933 49.5669L68.0647 76.8903L44.8401 60.0042L44.2521 59.5767L43.664 60.0042L20.4394 76.8903L29.3108 49.5669L29.5351 48.8762L28.9478 48.4492L5.72117 31.5617H34.43H35.1568L35.3812 30.8705L44.2521 3.5485Z"
                        stroke="#001780"></path>
                    <path
                        d="M44.0448 4.93771L54.065 31.3547L54.3098 32H55H57.5406L46.5175 60.4765L44.5547 59.1679L43.962 58.7728L43.3949 59.2038L20.9725 76.2449L29.9487 49.3162L30.1813 48.6183L29.5843 48.1885L7.10033 32H34H34.713L34.9454 31.326L44.0448 4.93771Z"
                        fill="#001780" stroke="#001780"></path>
                </g>
            </svg></i></span>
    <div class="carousel_24A">
        <div class="slick-slider slick-initialized" dir="ltr"><button type="button" data-role="none"
                class="slick-arrow slick-prev" style="display: block;"> Previous</button>
            <div class="slick-list">
                <div class="slick-track"
                    style="width: 9600px; opacity: 1; transform: translate3d(-3200px, 0px, 0px); transition: -webkit-transform 500ms ease 0s;">
                    <div data-index="-1" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true"
                        style="width: 640px;">
                        <div>
                            <article>
                                <h4>Best thing I’ve done in a while</h4>
                                <p>This is the most amazing mattress I ever had the pleasure to lay on.
                                    I don’t keep rolling over because of hip pain and my shoulders and
                                    back no longer hurt when I wake up. My cat also won’t get off the
                                    bed and sleep with me every night. My fiancé is already sleeping
                                    better and his back pain is getting better. 1000/10 stars.</p>
                                <strong>Cora F.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="0" class="slick-slide" tabindex="-1" aria-hidden="true"
                        style="outline: none; width: 640px;">
                        <div>
                            <article>
                                <h4>Like sleeping on air!</h4>
                                <p>After a lot of research, Nectar was the best value for a high quality
                                    product. I am pleased that my research paid of with such great
                                    dividends. I have a bad back and hips and have not had pain upon
                                    waking since I started sleeping on my Nectar mattress.</p>
                                <strong>Jennifer N.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="1" class="slick-slide" tabindex="-1" aria-hidden="true"
                        style="outline: none; width: 640px;">
                        <div>
                            <article>
                                <h4>Fantastic mattress. Remarkably sleeps cool. Exceptional value.
                                    Exemplary warranty.</h4>
                                <p>The top 3 reasons I love this mattress: 1. Best Bang for the Buck
                                    mattress with 2 free pillows included. 2. Amazing on the body. I
                                    finally can say I have had deep sleeps without waking up in the
                                    middle of the night. 3. Sleeps COOL. (I sleep hot. This is the most
                                    important reason I bought this mattress.)</p><strong>Javier
                                    R.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="2" class="slick-slide" tabindex="-1" aria-hidden="true"
                        style="outline: none; width: 640px;">
                        <div>
                            <article>
                                <h4>This is by far the best bed</h4>
                                <p>This is by far the BEST bed I have ever slept on !!! I am older and
                                    have a hard time sleeping all night, not to mention the back and
                                    knee pains. With this Bed and the Adjustable Frame I am finally
                                    sleeping and not in so much pain when I wake. LOVE THIS BED!!!!!</p>
                                <strong>Lisa G.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="3" class="slick-slide" tabindex="-1" aria-hidden="true"
                        style="outline: none; width: 640px;">
                        <div>
                            <article>
                                <h4>Best sleep I’ve ever had</h4>
                                <p>Listen. Plain and simple this is the best sleep I have ever gotten. I
                                    didn’t expect it to make this much of a difference. I fall asleep so
                                    quickly, and I feel so much better throughout the day. I used to
                                    have to wake up and crack my back in the mornings, but now I don’t
                                    have to. This mattress really has improved my quality of sleep 10X.
                                </p><strong>Joseph M.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="4" class="slick-slide slick-active slick-current" tabindex="-1" aria-hidden="false"
                        style="outline: none; width: 640px;">
                        <div>
                            <article>
                                <h4>Best investment ever</h4>
                                <p>Me and my husband love our king size mattress! Very comfortable and I
                                    love the firmness. Not too soft and not too firm. We had back pains
                                    from our previous bed and don't experience that anymore. My son said
                                    our bed was more comfortable than his and he has a serta mattress. I
                                    would recommend buying this.</p><strong>Aiesha M.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="5" class="slick-slide" tabindex="-1" aria-hidden="true"
                        style="outline: none; width: 640px;">
                        <div>
                            <article>
                                <h4>Very happy with mattress</h4>
                                <p>I loved how easy it was to order, receive and set up. The mattress is
                                    very affordable and very comfortable. My husband and I both have
                                    back issues and this has been an improvement over our last mattress!
                                </p><strong>Kathleen C.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="6" class="slick-slide" tabindex="-1" aria-hidden="true"
                        style="outline: none; width: 640px;">
                        <div>
                            <article>
                                <h4>Best thing I’ve done in a while</h4>
                                <p>This is the most amazing mattress I ever had the pleasure to lay on.
                                    I don’t keep rolling over because of hip pain and my shoulders and
                                    back no longer hurt when I wake up. My cat also won’t get off the
                                    bed and sleep with me every night. My fiancé is already sleeping
                                    better and his back pain is getting better. 1000/10 stars.</p>
                                <strong>Cora F.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="7" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true"
                        style="width: 640px;">
                        <div>
                            <article>
                                <h4>Like sleeping on air!</h4>
                                <p>After a lot of research, Nectar was the best value for a high quality
                                    product. I am pleased that my research paid of with such great
                                    dividends. I have a bad back and hips and have not had pain upon
                                    waking since I started sleeping on my Nectar mattress.</p>
                                <strong>Jennifer N.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="8" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true"
                        style="width: 640px;">
                        <div>
                            <article>
                                <h4>Fantastic mattress. Remarkably sleeps cool. Exceptional value.
                                    Exemplary warranty.</h4>
                                <p>The top 3 reasons I love this mattress: 1. Best Bang for the Buck
                                    mattress with 2 free pillows included. 2. Amazing on the body. I
                                    finally can say I have had deep sleeps without waking up in the
                                    middle of the night. 3. Sleeps COOL. (I sleep hot. This is the most
                                    important reason I bought this mattress.)</p><strong>Javier
                                    R.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="9" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true"
                        style="width: 640px;">
                        <div>
                            <article>
                                <h4>This is by far the best bed</h4>
                                <p>This is by far the BEST bed I have ever slept on !!! I am older and
                                    have a hard time sleeping all night, not to mention the back and
                                    knee pains. With this Bed and the Adjustable Frame I am finally
                                    sleeping and not in so much pain when I wake. LOVE THIS BED!!!!!</p>
                                <strong>Lisa G.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="10" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true"
                        style="width: 640px;">
                        <div>
                            <article>
                                <h4>Best sleep I’ve ever had</h4>
                                <p>Listen. Plain and simple this is the best sleep I have ever gotten. I
                                    didn’t expect it to make this much of a difference. I fall asleep so
                                    quickly, and I feel so much better throughout the day. I used to
                                    have to wake up and crack my back in the mornings, but now I don’t
                                    have to. This mattress really has improved my quality of sleep 10X.
                                </p><strong>Joseph M.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="11" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true"
                        style="width: 640px;">
                        <div>
                            <article>
                                <h4>Best investment ever</h4>
                                <p>Me and my husband love our king size mattress! Very comfortable and I
                                    love the firmness. Not too soft and not too firm. We had back pains
                                    from our previous bed and don't experience that anymore. My son said
                                    our bed was more comfortable than his and he has a serta mattress. I
                                    would recommend buying this.</p><strong>Aiesha M.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="12" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true"
                        style="width: 640px;">
                        <div>
                            <article>
                                <h4>Very happy with mattress</h4>
                                <p>I loved how easy it was to order, receive and set up. The mattress is
                                    very affordable and very comfortable. My husband and I both have
                                    back issues and this has been an improvement over our last mattress!
                                </p><strong>Kathleen C.</strong>
                            </article>
                        </div>
                    </div>
                    <div data-index="13" tabindex="-1" class="slick-slide slick-cloned" aria-hidden="true"
                        style="width: 640px;">
                        <div>
                            <article>
                                <h4>Best thing I’ve done in a while</h4>
                                <p>This is the most amazing mattress I ever had the pleasure to lay on.
                                    I don’t keep rolling over because of hip pain and my shoulders and
                                    back no longer hurt when I wake up. My cat also won’t get off the
                                    bed and sleep with me every night. My fiancé is already sleeping
                                    better and his back pain is getting better. 1000/10 stars.</p>
                                <strong>Cora F.</strong>
                            </article>
                        </div>
                    </div>
                </div>
            </div><button type="button" data-role="none" class="slick-arrow slick-next" style="display: block;">
                Next</button>
        </div>
    </div><a id="reviews_read_all_reviews" class="button_1Zo" href="/reviews">Read all reviews</a>
</section>